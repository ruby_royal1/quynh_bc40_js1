// BÀI 1: TÍNH TIỀN LƯƠNG
//K1:  Tạo hằng số luongNgay =100000, Người dùng nhập vào ngayLam
// K2: tiền lương = ngayLam*luongNgay
// K3: Hiển thị ra txtSalary


function tienLuong() {
    const luongNgay = 1 * 100000;
    var e = document.getElementById("ngayLam").value * luongNgay;
    document.getElementById("txtSalary").innerHTML = e;

}

// BÀI 2: TÍNH TRUNG BÌNH 5 SỐ
//K1:  Người dùng nhập vào num1, num2,..,num5
// K2: e= (num1+ num2+num3+num4+num5)/5
// K3: Hiển thị ra txtAverage

function trungBinh() {

    var e = 1 * document.getElementById("num1").value + 1 * document.getElementById("num2").value + 1 * document.getElementById("num3").value + 1 * document.getElementById("num4").value + 1 * document.getElementById("num5").value;
    document.getElementById("txtAverage").innerHTML = e / 5;

}
// BÀI 3: ĐỔI TIỀN
//K1:  Tạo hằng số tiGia=23500, Người dùng nhập vào moneyUSD
// K2: tiền đổi = moneyUSD*tiGia
// K3: Hiển thị ra txtMoney

function doiTien() {
    const tiGia = 1 * 23500;
    var e = document.getElementById("moneyUSD").value * tiGia;
    document.getElementById("txtMoney").innerHTML = e;

}
// BÀI 4: TÍNH DIỆN TÍCH, CHU VI HÌNH CHỮ NHẬT
//K1:  Người dùng nhập vào cDai, cRong;
// K2: chuvi = 2*cDai+2*cRong; dientich=cDai*cRong
// K3: Hiển thị ra txtChuvi, txtDientich
function chuviDientich() {

    var e = 2 * document.getElementById("cDai").value + 2 * document.getElementById("cRong").value;
    document.getElementById("txtChuvi").innerHTML = e;
    document.getElementById("txtDientich").innerHTML = document.getElementById("cDai").value * document.getElementById("cRong").value;

}

// BÀI 5: TÍNH TỔNG 2 KÝ SỐ
//K1:  Người dùng nhập vào số có 2 chữ số;
// K2: dvi=so%10; chuc=(so-so%10)/10
// K3: Hiển thị ra txtTong
function tongKs() {

    var e = 1 * document.getElementById("so").value;
    var dvi = e % 10;
    var chuc = (e - e % 10) / 10;
    document.getElementById("txtTong").innerHTML = dvi + chuc;

}